package com.tsc.jarinchekhina.tm.api.repository;

import com.tsc.jarinchekhina.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    void add(Task task);

    void remove(Task task);

    void clear();

    void removeAllByProjectId(String projectId);

    List<Task> findAll();

    List<Task> findAllByProjectId(String projectId);

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Task findOneByName(String name);

    Task removeOneById(String id);

    Task removeOneByIndex(Integer index);

    Task removeOneByName(String name);

}
