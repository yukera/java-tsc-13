package com.tsc.jarinchekhina.tm.api.controller;

public interface IProjectController {

    void showProjects();

    void clearProjects();

    void createProject();

    void showProjectById();

    void showProjectByIndex();

    void showProjectByName();

    void updateProjectById();

    void updateProjectByIndex();

    void removeProjectById();

    void removeProjectByIndex();

    void removeProjectByName();

    void startProjectById();

    void startProjectByIndex();

    void startProjectByName();

    void finishProjectById();

    void finishProjectByIndex();

    void finishProjectByName();

    void changeProjectStatusById();

    void changeProjectStatusByIndex();

    void changeProjectStatusByName();

}
