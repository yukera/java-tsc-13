package com.tsc.jarinchekhina.tm.repository;

import com.tsc.jarinchekhina.tm.api.repository.ITaskRepository;
import com.tsc.jarinchekhina.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    private List<Task> tasks = new ArrayList<>();

    @Override
    public void add(final Task task) {
        tasks.add(task);
    }

    @Override
    public void remove(final Task task) {
        tasks.remove(task);
    }

    @Override
    public List<Task> findAll() {
        return tasks;
    }

    @Override
    public void clear() {
        tasks.clear();
    }

    @Override
    public Task findOneById(final String id) {
        for (final Task task : tasks) {
            if (id.equals(task.getId())) return task;
        }
        return null;
    }

    @Override
    public Task findOneByIndex(final Integer index) {
        return tasks.get(index);
    }

    @Override
    public Task findOneByName(final String name) {
        for (final Task task : tasks) {
            if (name.equals(task.getName())) return task;
        }
        return null;
    }

    @Override
    public Task removeOneById(final String id) {
        final Task task = findOneById(id);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public Task removeOneByIndex(final Integer index) {
        final Task task = findOneByIndex(index);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public Task removeOneByName(final String name) {
        final Task task = findOneByName(name);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public List<Task> findAllByProjectId(final String projectId) {
        final List<Task> tasksByProject = new ArrayList<>();
        for (final Task task : tasks) {
            if (projectId.equals(task.getProjectId())) tasksByProject.add(task);
        }
        if (tasksByProject.size() > 0) return tasksByProject;
        else return null;
    }

    @Override
    public void removeAllByProjectId(final String projectId) {
        List<Task> tasksNew = new ArrayList<>();
        for (final Task task : tasks) {
            if (!projectId.equals(task.getProjectId())) tasksNew.add(task);;
        }
        tasks = tasksNew;
    }

}
