package com.tsc.jarinchekhina.tm.component;

import com.tsc.jarinchekhina.tm.api.controller.ICommandController;
import com.tsc.jarinchekhina.tm.api.controller.IProjectController;
import com.tsc.jarinchekhina.tm.api.controller.ITaskController;
import com.tsc.jarinchekhina.tm.api.repository.ICommandRepository;
import com.tsc.jarinchekhina.tm.api.repository.IProjectRepository;
import com.tsc.jarinchekhina.tm.api.repository.ITaskRepository;
import com.tsc.jarinchekhina.tm.api.service.ICommandService;
import com.tsc.jarinchekhina.tm.api.service.IProjectService;
import com.tsc.jarinchekhina.tm.api.service.IProjectTaskService;
import com.tsc.jarinchekhina.tm.api.service.ITaskService;
import com.tsc.jarinchekhina.tm.constant.ArgumentConst;
import com.tsc.jarinchekhina.tm.constant.TerminalConst;
import com.tsc.jarinchekhina.tm.controller.CommandController;
import com.tsc.jarinchekhina.tm.controller.ProjectController;
import com.tsc.jarinchekhina.tm.controller.TaskController;
import com.tsc.jarinchekhina.tm.repository.CommandRepository;
import com.tsc.jarinchekhina.tm.repository.ProjectRepository;
import com.tsc.jarinchekhina.tm.repository.TaskRepository;
import com.tsc.jarinchekhina.tm.service.CommandService;
import com.tsc.jarinchekhina.tm.service.ProjectService;
import com.tsc.jarinchekhina.tm.service.ProjectTaskService;
import com.tsc.jarinchekhina.tm.service.TaskService;
import com.tsc.jarinchekhina.tm.util.TerminalUtil;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final ITaskRepository taskRepository = new TaskRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);

    private final IProjectController projectController = new ProjectController(projectService, projectTaskService);

    private final ITaskController taskController = new TaskController(taskService, projectTaskService);

    public void start(final String[] args) {
        System.out.println("*** Welcome to Task Manager ***");
        parseArgs(args);
        while (true) {
            System.out.println("ENTER COMMAND:");
            parseCommand(TerminalUtil.nextLine());
        }
    }

    public void parseArgs(final String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        parseArg(arg);
        System.exit(0);
    }

    public void parseArg(final String arg) {
        switch (arg) {
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.INFO:
                commandController.showInfo();
                break;
            case ArgumentConst.COMMANDS:
                commandController.showCommands();
                break;
            case ArgumentConst.ARGUMENTS:
                commandController.showArguments();
                break;
            default:
                commandController.showErrorArgument();
        }
    }

    public void parseCommand(final String command) {
        switch (command) {
            case TerminalConst.ABOUT:
                commandController.showAbout();
                break;
            case TerminalConst.VERSION:
                commandController.showVersion();
                break;
            case TerminalConst.HELP:
                commandController.showHelp();
                break;
            case TerminalConst.INFO:
                commandController.showInfo();
                break;
            case TerminalConst.COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConst.EXIT:
                commandController.exit();
                break;
            case TerminalConst.TASK_CLEAR:
                taskController.clearTasks();
                break;
            case TerminalConst.TASK_CREATE:
                taskController.createTask();
                break;
            case TerminalConst.TASK_LIST:
                taskController.showTasks();
                break;
            case TerminalConst.TASK_VIEW_BY_ID:
                taskController.showTaskById();
                break;
            case TerminalConst.TASK_VIEW_BY_INDEX:
                taskController.showTaskByIndex();
                break;
            case TerminalConst.TASK_VIEW_BY_NAME:
                taskController.showTaskByName();
                break;
            case TerminalConst.TASK_UPDATE_BY_ID:
                taskController.updateTaskById();
                break;
            case TerminalConst.TASK_UPDATE_BY_INDEX:
                taskController.updateTaskByIndex();
                break;
            case TerminalConst.TASK_REMOVE_BY_ID:
                taskController.removeTaskById();
                break;
            case TerminalConst.TASK_REMOVE_BY_INDEX:
                taskController.removeTaskByIndex();
                break;
            case TerminalConst.TASK_REMOVE_BY_NAME:
                taskController.removeTaskByName();
                break;
            case TerminalConst.TASK_START_BY_ID:
                taskController.startTaskById();
                break;
            case TerminalConst.TASK_START_BY_INDEX:
                taskController.startTaskByIndex();
                break;
            case TerminalConst.TASK_START_BY_NAME:
                taskController.startTaskByName();
                break;
            case TerminalConst.TASK_FINISH_BY_ID:
                taskController.finishTaskById();
                break;
            case TerminalConst.TASK_FINISH_BY_INDEX:
                taskController.finishTaskByIndex();
                break;
            case TerminalConst.TASK_FINISH_BY_NAME:
                taskController.finishTaskByName();
                break;
            case TerminalConst.TASK_STATUS_CHANGE_BY_ID:
                taskController.changeTaskStatusById();
                break;
            case TerminalConst.TASK_STATUS_CHANGE_BY_INDEX:
                taskController.changeTaskStatusByIndex();
                break;
            case TerminalConst.TASK_STATUS_CHANGE_BY_NAME:
                taskController.changeTaskStatusByName();
                break;
            case TerminalConst.TASK_BIND_TO_PROJECT:
                taskController.bindTaskByProjectId();
                break;
            case TerminalConst.TASK_UNBIND_FROM_PROJECT:
                taskController.unbindTaskByProjectId();
                break;
            case TerminalConst.TASK_VIEW_BY_PROJECT_ID:
                taskController.findAllTaskByProjectId();
                break;
            case TerminalConst.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case TerminalConst.PROJECT_CREATE:
                projectController.createProject();
                break;
            case TerminalConst.PROJECT_LIST:
                projectController.showProjects();
                break;
            case TerminalConst.PROJECT_VIEW_BY_ID:
                projectController.showProjectById();
                break;
            case TerminalConst.PROJECT_VIEW_BY_INDEX:
                projectController.showProjectByIndex();
                break;
            case TerminalConst.PROJECT_VIEW_BY_NAME:
                projectController.showProjectByName();
                break;
            case TerminalConst.PROJECT_UPDATE_BY_ID:
                projectController.updateProjectById();
                break;
            case TerminalConst.PROJECT_UPDATE_BY_INDEX:
                projectController.updateProjectByIndex();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_ID:
                projectController.removeProjectById();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_INDEX:
                projectController.removeProjectByIndex();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_NAME:
                projectController.removeProjectByName();
                break;
            case TerminalConst.PROJECT_START_BY_ID:
                projectController.startProjectById();
                break;
            case TerminalConst.PROJECT_START_BY_INDEX:
                projectController.startProjectByIndex();
                break;
            case TerminalConst.PROJECT_START_BY_NAME:
                projectController.startProjectByName();
                break;
            case TerminalConst.PROJECT_FINISH_BY_ID:
                projectController.finishProjectById();
                break;
            case TerminalConst.PROJECT_FINISH_BY_INDEX:
                projectController.finishProjectByIndex();
                break;
            case TerminalConst.PROJECT_FINISH_BY_NAME:
                projectController.finishProjectByName();
                break;
            case TerminalConst.PROJECT_STATUS_CHANGE_BY_ID:
                projectController.changeProjectStatusById();
                break;
            case TerminalConst.PROJECT_STATUS_CHANGE_BY_INDEX:
                projectController.changeProjectStatusByIndex();
                break;
            case TerminalConst.PROJECT_STATUS_CHANGE_BY_NAME:
                projectController.changeProjectStatusByName();
                break;
            default:
                commandController.showErrorCommand();
        }
    }

}
